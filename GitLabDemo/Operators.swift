//
//  Operators.swift
//  GitLabDemo
//
//  Created by DEFTeam on 07/08/20.
//  Copyright © 2020 com.defteam. All rights reserved.
//

import Foundation

struct Operators {
    var a: Int
    var b: Int
    
    func addition() -> Int {
        return a + b
    }
    
    func substraction() -> Int {
        return a - b
    }
    
    func Multiplication() -> Int {
        return a * b
    }
}
