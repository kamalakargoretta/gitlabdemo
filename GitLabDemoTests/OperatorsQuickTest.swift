//
//  OperatorsQuickTest.swift
//  GitLabDemoTests
//
//  Created by DEFTeam on 08/08/20.
//  Copyright © 2020 com.defteam. All rights reserved.
//

import Quick
import Nimble
@testable import GitLabDemo

class OperatorsQuickTest: QuickSpec {

    override func spec() {
        var operators: Operators!
        
        describe("Math operation") {
            context("valid addtion") {
                afterEach {
                    operators = nil
                }
                beforeEach {
                    operators = Operators(a: 5, b: 10)
                }
                it("the addition of 5 and 10 eqaul to 15") {
                    expect(operators.addition()).to(equal(15))
                }
            }
        }
    }

}
