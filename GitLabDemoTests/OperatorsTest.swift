//
//  OperatorsTest.swift
//  GitLabDemoTests
//
//  Created by DEFTeam on 07/08/20.
//  Copyright © 2020 com.defteam. All rights reserved.
//

import XCTest
@testable import GitLabDemo

class OperatorsTest: XCTestCase {

    var operators: Operators?
    override func setUp() {
        operators = Operators(a: 5, b: 10)
    }

    override func tearDown() {
        operators = nil
    }
    
    func testAddition() {
        let result = operators?.addition()
        XCTAssertEqual(result, 15, "5 plus 10 should be 15")
    }

    func testSubstraction() {
       let result = operators?.substraction()
       XCTAssertEqual(result, -5, "5 plus 10 should be -5")
    }
    
    func testMultiplication() {
       let result = operators?.Multiplication()
       XCTAssertEqual(result, 50, "5 plus 10 should be 50")
    }
}
